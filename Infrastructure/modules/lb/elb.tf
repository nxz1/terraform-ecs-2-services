resource "aws_lb" "my_lb" {
  name               = "${var.loadbalancer_name}"
  load_balancer_type = "application"
  internal           = false
  subnets            = "${var.subnets}"
  security_groups = [aws_security_group.lb.id]
}

resource "aws_security_group" "lb" {
  name   = "${var.loadbalancer_sg_name}"
  vpc_id = "${var.vpc_id}"
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb_listener" "web_listener" {
  load_balancer_arn = aws_lb.my_lb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.service_1_target_group.arn
  }
}

resource "aws_lb_target_group" "service_1_target_group" {
  name        = "${var.service_1_TG_name}"
  port        = "80"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"
  stickiness {
    type = "lb_cookie"
    enabled = false
  }
  health_check {
    path                = "/"
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 60
    interval            = 300
    matcher             = "404"
  }
}

resource "aws_lb_target_group" "service_2_target_group" {
  name        = "${var.service_2_TG_name}"
  port        = "80"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"
  stickiness {
    type = "lb_cookie"
    enabled = false
  }
  health_check {
    path                = "/"
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 60
    interval            = 300
    matcher             = "200"
  }
}

resource "aws_lb_listener_rule" "service_1_alb_rule" {
  listener_arn = aws_lb_listener.web_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.service_1_target_group.arn
  }

  condition {
    host_header {
      values = ["hotel-api.nxz.com"]
    }
  }
  
}

resource "aws_lb_listener_rule" "service_2_alb_rule" {
  listener_arn = aws_lb_listener.web_listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.service_2_target_group.arn
  }

  condition {
    host_header {
      values = ["portfolio.nxz.com"]
    }
  }
  
}