variable "ecs_cluster_name" {}
variable "container_insights" {}
variable "managed_termination_protection" {}
variable "task_definition_family_1_name" {}
variable "task_definition_1_family_1_container_def_path" {}

variable "cw_log_group_name" {}
variable "asg_arn" {}

variable "service_1_target_group_arn" {}
variable "service_2_target_group_arn" {}

variable "web_listener_group" {}
variable "listener_arn" {}
variable "environment_name" {}

variable "capacity_provider_name" {}
# 2nd App
variable "task_definition_family_2_name" {}
variable "task_definition_2_family_2_container_def_path" {}
variable "service_2_name" {}
variable "service_2_container_name" {}
variable "service_2_container_port" {}

variable "service_1_container_port" {
    type = number
}


variable "service_1_name" {}
variable "service_1_container_name" {}