output "service_1_name" {
    value = "${aws_ecs_service.service_1.name}"
}
output "service_2_name" {
    value = "${aws_ecs_service.service_2.name}"
}

# output "service_1_container_name" {
#     value = "${aws_ecs_service.service_1.load_balancer.container_name}"
# }
# output "service_1_container_port" {
#     value = "${aws_ecs_service.service_1.load_balancer.container_port}"
# }

# output "service_2_container_name" {
#     value = "${aws_ecs_service.service_2.load_balancer.container_name}"
# }
# output "service_2_container_port" {
#     value = "${aws_ecs_service.service_2.load_balancer.container_port}"
# }