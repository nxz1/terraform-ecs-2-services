resource "aws_ecs_cluster" "my_cluster" {
  name = "${var.environment_name}-${var.ecs_cluster_name}"

  setting {
    name  = "containerInsights"
    value = "${var.container_insights}"
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.cw_log_group_name}"
  tags = {
    "env"       = "${var.environment_name}-cw-group"
  }
}

resource "aws_ecs_capacity_provider" "my_capacity_provider" {
  name = "${var.capacity_provider_name}"
  auto_scaling_group_provider {
    auto_scaling_group_arn         = "${var.asg_arn}"
    managed_termination_protection = "${var.managed_termination_protection}"

    managed_scaling {
      status          = "ENABLED"
      target_capacity = 85
    }
  }
}

data "aws_lb_listener" "web_listener" {
  arn = var.listener_arn
}

resource "aws_ecs_task_definition" "task_definition_1" {
  family                = "${var.task_definition_family_1_name}"
  container_definitions = file("${var.task_definition_1_family_1_container_def_path}")
  network_mode          = "bridge"
  tags = {
    "env"       = "dev"
    "createdBy" = "Nawaz"
  }
}

resource "aws_ecs_task_definition" "task_definition_2" {
  family                = "${var.task_definition_family_2_name}"
  container_definitions = file("${var.task_definition_2_family_2_container_def_path}")
  network_mode          = "bridge"
  tags = {
    "env"       = "dev"
    "createdBy" = "Nawaz"
  }
}

resource "aws_ecs_service" "service_1" {
  name            = "${var.service_1_name}"
  cluster         = aws_ecs_cluster.my_cluster.id
  task_definition = aws_ecs_task_definition.task_definition_1.arn
  enable_ecs_managed_tags = true
  propagate_tags = "SERVICE"
  desired_count   = 1
  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }
  load_balancer {
    target_group_arn = "${var.service_1_target_group_arn}"
    container_name   = "${var.service_1_container_name}"
    container_port   = "${var.service_1_container_port}"
  }
  # Optional: Allow external changes without Terraform plan difference(for example ASG)
  lifecycle {
    ignore_changes = [desired_count]
  }
  launch_type = "EC2"
  depends_on = [data.aws_lb_listener.web_listener]
}

resource "aws_ecs_service" "service_2" {
  name            = "${var.service_2_name}"
  cluster         = aws_ecs_cluster.my_cluster.id
  task_definition = aws_ecs_task_definition.task_definition_2.arn
  enable_ecs_managed_tags = true
  propagate_tags = "TASK_DEFINITION"
  desired_count   = 1
  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }
  load_balancer {
    target_group_arn = "${var.service_2_target_group_arn}"
    container_name   = "${var.service_2_container_name}"
    container_port   = "${var.service_2_container_port}"
  }
  # Optional: Allow external changes without Terraform plan difference(for example ASG)
  lifecycle {
    ignore_changes = [desired_count]
  }
  launch_type = "EC2"
  depends_on = [data.aws_lb_listener.web_listener]
}