
output "asg_arn" {
  value = aws_autoscaling_group.asg.arn
}

output "ecs_instance_sg" {
  value = "${aws_security_group.ec2-sg.id}"
}