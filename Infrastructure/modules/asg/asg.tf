data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami*amazon-ecs-optimized"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["amazon", "self"]
}

# Security Group
resource "aws_security_group" "ec2-sg" {
  name        = "${var.ecs_instance_sg_name}"
  description = "allow all"
  vpc_id      = "${var.ec2_vpc_id}"
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "test"
  }
}

# Launch Configuration
resource "aws_launch_configuration" "lc" {
  name          = "${var.ecs_instance_lc_name}"
  image_id      = data.aws_ami.amazon_linux.id
  instance_type = "t2.medium"
  lifecycle {
    create_before_destroy = true
  }
  iam_instance_profile        = "${var.iam_instance_profile}"
  key_name                    = "${var.ecs_instance_key_name}"
  security_groups             = [aws_security_group.ec2-sg.id]
  associate_public_ip_address = true
  user_data                   = <<EOF
#! /bin/bash
sudo apt-get update
sudo echo "ECS_CLUSTER=${var.environment_name}-${var.ecs_cluster_name}" >> /etc/ecs/ecs.config;
EOF
}

# Auto Scaling Group
resource "aws_autoscaling_group" "asg" {
  name                      = "${var.ecs_asg_name}"
  launch_configuration      = aws_launch_configuration.lc.name
  min_size                  = 2
  max_size                  = 2
  desired_capacity          = 2
  health_check_type         = "ELB"
  health_check_grace_period = 300
  vpc_zone_identifier       = "${var.vpc_zone_identifier}"
  target_group_arns     = ["${var.service_1_target_group_arn}", "${var.service_2_target_group_arn}"]
  protect_from_scale_in = true
  lifecycle {
    create_before_destroy = true
  }
  tag {
    key                 = "AmazonECSManaged"
    value               = true
    propagate_at_launch = true
  }
}
