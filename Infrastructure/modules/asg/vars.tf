variable "ec2_vpc_id" {}
variable "ecs_cluster_name" {}
variable "vpc_zone_identifier" {}
variable "iam_instance_profile" {}
variable "ecs_instance_sg_name" {}
variable "ecs_instance_lc_name" {}
variable "ecs_instance_key_name" {}
variable "environment_name" {}
variable "service_1_target_group_arn" {}
variable "service_2_target_group_arn" {}

variable "ecs_asg_name" {}