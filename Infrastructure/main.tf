provider "aws" {
    region = "${var.aws_region}"
    profile = "${var.aws_profile}"
}

 terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

module "networking" {
    source = "./modules/networking"
    name = "${var.environment_name}"
    vpc_cidr = [ "${var.vpc_cidr}" ]
}

module "iam" {
    source = "./modules/iam"
    ecs_instance_role_name = "${var.ecs_instance_role_name}"
}

module "ecs_cluster" {
    # Cluster Settings
    source = "./modules/ecs"
    environment_name = "${var.environment_name}"
    ecs_cluster_name = "${var.ecs_cluster_name}"
    container_insights = "${var.container_insights}"
    managed_termination_protection = "${var.termination_protection}"
    capacity_provider_name = "${var.capacity_provider_name}"
    cw_log_group_name = "${var.cw_log_group_name}"
    
    # Capacity Provider
    listener_arn = "${module.alb.web_listener_arn}"
    asg_arn = "${module.asg.asg_arn}" 
    web_listener_group = "${module.alb.web_listener_group}"

    # Service 1 Settings
    task_definition_family_1_name = "${var.task_definition_family_1_name}"
    task_definition_1_family_1_container_def_path = "${var.task_definition_1_family_1_container_def_path}"

    service_1_name = "${var.service_1_name}"
    service_1_target_group_arn = "${module.alb.service_1_target_group_arn}"
    service_1_container_name = "${var.service_1_container_name}"
    service_1_container_port = "${var.service_1_container_port}"

    # Service 2 Settings
    task_definition_family_2_name = "${var.task_definition_family_2_name}"
    task_definition_2_family_2_container_def_path = "${var.task_definition_2_family_2_container_def_path}"

    service_2_name = "${var.service_2_name}"
    service_2_container_name = "${var.service_2_container_name}"
    service_2_container_port = "${var.service_2_container_port}"
    service_2_target_group_arn = "${module.alb.service_2_target_group_arn}"
}

module "asg" {
    source = "./modules/asg"
    environment_name = "${var.environment_name}"
    ecs_cluster_name = "${var.ecs_cluster_name}"
    ecs_instance_sg_name = "${var.ecs_instance_sg_name}"
    ecs_instance_lc_name = "${var.ecs_instance_lc_name}"
    ecs_instance_key_name = "${var.ecs_instance_key_name}"
    ecs_asg_name = "${var.ecs_asg_name}"
    ec2_vpc_id = "${module.networking.vpc_id}"
    vpc_zone_identifier = "${module.networking.private_subnets}"
    iam_instance_profile = "${module.iam.ecs_instance_iam_service_role}"
    service_1_target_group_arn = "${module.alb.service_1_target_group_arn}"
    service_2_target_group_arn = "${module.alb.service_2_target_group_arn}"
}

module "alb" {
    source = "./modules/lb"
    # LB Settings
    loadbalancer_name = "${var.environment_name}-${var.lb_name}"
    loadbalancer_sg_name = "${var.environment_name}-${var.lb_sg_name}"
    vpc_id = "${module.networking.vpc_id}"
    subnets = "${module.networking.public_subnets}"

    # Target Group Settings
    service_1_TG_name = "${var.service_1_TG_name}"
    service_2_TG_name = "${var.service_2_TG_name}"
    
}