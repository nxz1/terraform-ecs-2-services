# AWS Config
aws_region = "eu-west-2"
aws_profile = "nawaz@itfreak"
environment_name = "dev"

# VPC
vpc_cidr = "200.100.0.0/16"
# To configure subnets go to the module and modify public_subnets and private_subnets resources.

# ASG
ecs_instance_sg_name = "ecs-cluster-allow-all-inbound-ec2"
ecs_instance_lc_name = "ecs-cluster-t2-medium-lc"
ecs_instance_key_name = "itfreak-dev-key"
ecs_asg_name = "ecs-autoscaling-group"

# ECS Cluster
capacity_provider_name = "my-custom-capacity-provider"
ecs_cluster_name = "ecs-cluster-1"
container_insights = "enabled"
termination_protection = "DISABLED"
cw_log_group_name = "/ecs/ecs-cluster-logging"

# Service 1
task_definition_family_1_name = "service-1-task-definition"
task_definition_1_family_1_container_def_path = "container-definitions/hotel-service-api.json"
service_1_name = "service-1"
service_1_container_name = "hotel-rating-api-container"
service_1_container_port = 8090

# Service 2
task_definition_family_2_name = "service-2-task-definition"
task_definition_2_family_2_container_def_path = "container-definitions/portfolio.json"
service_2_name = "portfolio-service" # Default
service_2_container_name = "portfolio-website-container" # Must match with container_name in Container Definitions.
service_2_container_port = "80" # Must match with container_name in Container Definitions.

# ELB
lb_name = "ecs-cluster-lb"
lb_sg_name = "ecs-cluster-lb-sg"
service_1_TG_name = "service-1-tg-HTTP"
service_2_TG_name = "portfolio-website-tg-HTTP"

# IAM
ecs_instance_role_name = "ecsInstanceRole"
