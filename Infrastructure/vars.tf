
variable "aws_region" {}
variable "aws_profile" {}

# VPC
variable "environment_name" {}
variable "vpc_cidr" {}

# IAM
variable "ecs_instance_role_name" {}

# ASG
variable "ecs_instance_sg_name" {}
variable "ecs_instance_lc_name" {}
variable "ecs_instance_key_name" {}
variable "ecs_asg_name" {}

# ECS Cluster 
variable "ecs_cluster_name" {}
variable "container_insights" {}
variable "termination_protection" {}
variable "cw_log_group_name" {}
variable "capacity_provider_name" {}

# ECS Service 1
variable "task_definition_family_1_name" {}
variable "task_definition_1_family_1_container_def_path" {}
variable "service_1_name" {}
variable "service_1_container_name" {}
variable "service_1_container_port" {
    type = number
}

# ECS Service 2
variable "task_definition_family_2_name" {}
variable "task_definition_2_family_2_container_def_path" {}
variable "service_2_name" {}
variable "service_2_container_name" {}
variable "service_2_container_port" {
    type = number
}

# ALB
variable "lb_name" {}
variable "lb_sg_name" {}
variable "service_1_TG_name" {}
variable "service_2_TG_name" {}