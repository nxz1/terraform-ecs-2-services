output "vpc_id" {
  value = module.networking.vpc_id
}

output "public_subnets" {
  value = module.networking.public_subnets
}

output "private_subnets" {
  value = module.networking.private_subnets
}

output "alb_dns" {
  value = module.alb.dns_name
}

output "igw_id" {
  value = module.networking.igw_id
}

# Services
output "service_1_name" {
  value = module.ecs_cluster.service_1_name
}
output "service_2_name" {
  value = module.ecs_cluster.service_2_name
}