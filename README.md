# EC2 Container Services Demo Using Terraform

### Architecture Diagram
![image](Asset/AWS-ECS-Demo-Services.png)

### About this project
This terraform project will spin up an ECS cluster from scratch with a new vpc, 2 public and private subnets, 1 Application load balancer, 1 Autoscaling group, 2 target groups, IAM roles, Cloudwatch logging, 2 task definitions.
It will then deploy 2 services defined in container-definitions/*.json.

**In order to test it on your local system, build your own image using In-Memory/MySQL database from the given [code](Code/README.md).**

If you want to use a proper database eg MySQL, follow the instructions below.

### Step 1: Create a RDS MYSQL 5.7.34 Instance
    Create a MYSQL 5.7.34 RDS instance and get the following information with you.
    - Endpoint
    - Username
    - Password

### Step 2: Clone the Repository
    git clone https://gitlab.com/nxz1/terraform-ecs-2-services.git

### Step 3: Update the DB connection string
#### Changes
##### 1. File: Code/src/main/resources/application.yaml
    Update the following information.
        url: jdbc:mysql://<DB_HOST_URL>/<DB_NAME>?useSSL=false
        username: <DB_USER>
        password: <DB_PASSWORD>
file reference
![image](Asset/database-connection-string.png)

### Step 4a: Build the docker image with your repository tag
    docker build --no-cache -t remote_id/hotel-rating-api:0.1 .

### Step 4b: (Optional) Create dummy data
   - docker run -itd -p 8090:8090 -p 8091:8091 remote_id/hotel-rating-api:0.1
   - Create some documents on database using POST request [Refer here](Code/README.md) 
    
### Step 5: Push image to remote repository
    - docker push remote_id/hotel-rating-api:0.1

### Step 6: Bring up the infra
#### Changes in file
#####  1. Infrastructure/Environment/dev.tfvars
    Required change:
    1. aws_region --> Your AWS Region
    2. aws_profile --> Your AWS Profile
    3. ecs_instance_key_name --> AWS Key must be present in the above region mentioned
file reference
![image](Asset/setup-application-environment.png)

##### 2. Infrastructure/container-definitions/hotel-service-api.json
    Required Change:
    1. image --> Image URL  i.e remote_id/hotel-rating-api:0.1
file reference
![image](Asset/container-definition-example.png)

Once done, run the terraform commands
- cd Infrastructure
- terraform init
- terraform plan -var-file Environment/dev.tfvars
- terraform apply -var-file Environment/dev.tfvars

#### If everything goes right and Infrastructure comes up, get the IP address of ALB using any tool of choice. I prefer dig.
#### Update your host entry with the given host rules like shown below
#### File path
    ##### For Linux: /etc/hosts
    ##### For Windows: C:\System32\drivers\etc\hosts
file reference
![image](Asset/host-entries.png)

### Now You can use your applications
### To know more about the REST Api deployed in this project [Refer here](Code/README.md) 
